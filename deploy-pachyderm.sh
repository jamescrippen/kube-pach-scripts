#!/bin/bash

RESOURCE_GROUP="USC-K8S-SBOX-RG"
STORAGE_ACCOUNT="usscpachystorage"
STORAGE_SIZE=10
CONTAINER_NAME="pachyderm"
LOCATION="southcentralus"

# Create Azure repo and install Azure CLI 2.0
echo "Checking if Azure CLI is already installed"
if [ -x "$(command -v az)" ]; then
    ver=`az --version | head -1`
    echo "$ver found, continuing"
else
    echo "Azure CLI Not found, installing ..."
    AZ_REPO=$(lsb_release -cs)
    echo "deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ $AZ_REPO main" | \
        sudo tee /etc/apt/sources.list.d/azure-cli.list

    curl -L https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -

    sudo apt-get install apt-transport-https
    sudo apt-get update && sudo apt-get install azure-cli
fi

read -sp "Azure Username:  " AZ_USER && echo && read -sp "Azure Password: " AZ_PASS && echo && az login -u $AZ_USER -p $AZ_PASS

# Create an Azure storage account for pachyderm
az storage account create \
  --resource-group="${RESOURCE_GROUP}" \
  --location="${LOCATION}" \
  --sku=Standard_LRS \
  --name="${STORAGE_ACCOUNT}" \
  --kind=Storage

# Get Storage account key
STORAGE_KEY="$(az storage account keys list \
            --account-name="${STORAGE_ACCOUNT}" \
            --resource-group="${RESOURCE_GROUP}" \
            --output=json \
            | jq '.[0].value' -r
        )"

# Install pachctl and deploy pachyderm to local kubernetes cluster
echo "Checking for pachctl"
if [ -x "$(command -v pachctl)" ]; then
    ver=`pachctl version --client-only`
    echo "Found $ver"
else
    echo "pachctl not found, installing ..."
    curl -o /tmp/pachctl.deb -L https://github.com/pachyderm/pachyderm/releases/download/v1.7.6/pachctl_1.7.6_amd64.deb && sudo dpkg -i /tmp/pachctl.deb
fi

pachctl version --client-only

pachctl deploy microsoft ${CONTAINER_NAME} ${STORAGE_ACCOUNT} ${STORAGE_KEY} ${STORAGE_SIZE} --dynamic-etcd-nodes 1 --no-guaranteed