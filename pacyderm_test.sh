echo; echo "Deleting everything..."
yes | pachctl delete-all
# Actually should only delete things belonging to this pipeline as currently deployed, if any
# So deployment needs to keep track of what it creates for later deletion

echo; echo "Creating repo lookup-names..."
pachctl create-repo data-sync__lookup-names
pachctl start-commit data-sync__lookup-names master
for name in LK_Event_Results LK_Hit_Trajectories LK_Leagues LK_Levels LK_Pitch_Results LK_Pitch_Types LK_Schedule_Types Projections_LWTS
do
  (date; cat /proc/sys/kernel/random/uuid) | pachctl put-file data-sync__lookup-names master ${name} -f -
done
pachctl finish-commit data-sync__lookup-names master

echo; echo "Creating repo delta-names..."
pachctl create-repo data-sync__delta-names
pachctl start-commit data-sync__delta-names master
for name in Events Hits Pitcher_Max_Velo Pitches Pitches_Attributes Players Schedule Teams
do
  (date; cat /proc/sys/kernel/random/uuid) | pachctl put-file data-sync__delta-names master ${name} -f -
done
pachctl finish-commit data-sync__delta-names master

echo; echo "Creating repo git-1..."
pachctl create-repo data-sync__git-1
pachctl start-commit data-sync__git-1 master
mkdir temp-$$
cd temp-$$
git clone git@bitbucket.org:on3technologies/pipeline-datasync.git
cd pipeline-datasync
for name in $(find data-sync-mirror -type f)
do
  pachctl put-file data-sync__git-1 master $name -f $name
done
pachctl finish-commit data-sync__git-1 master
cd ../..
rm -rf temp-$$

echo; echo "Creating pipeline start..."
pachctl create-pipeline -f - <<'END'
{
  "pipeline": {
    "name": "data-sync__start"
  },
  "input": {
    "cron": {
      "name": "trigger",
      "spec": "@every 15m"
    }
  },
  "transform": {
    "env": {
      "DB_CONFIG": "dev"
    },
    "cmd": [ "sh" ],
    "stdin": [
      "if [ -r /pfs/trigger/time ]; then",
      "  echo \"$(date) --- STARTING ---\" >/pfs/out/trigger",
      "fi"
    ]
  }
}
END

echo; echo "Creating pipeline sync-lookups..."
pachctl create-pipeline -f - <<'END'
{
  "pipeline": {
    "name": "data-sync__sync-lookups"
  },
  "parallelism_spec": {
    "constant": 2
  },
  "input": {
    "cross": [
      {
        "atom": {
          "name": "trigger",
          "repo": "data-sync__start",
          "glob": "/"
        }
      }, {
        "atom": {
          "name": "names",
          "repo": "data-sync__lookup-names",
          "glob": "/*"
        }
      }, {
        "atom": {
          "name": "scripts",
          "repo": "data-sync__git-1",
          "glob": "/"
        }
      }
    ]
  },
  "transform": {
    "env": {
      "DB_CONFIG": "dev",
      "TABLE_TYPE": "lookup"
    },
    "cmd": [ "sh" ],
    "stdin": [
      "if [ -r /pfs/trigger/trigger ]; then",
      "  (",
      "    cd /pfs/scripts/data-sync-mirror",
      "    echo npm install",
      "    for NAME in $(ls /pfs/names); do",
      "      echo $(date) node sync-${TABLE_TYPE}.js db-config-${DB_CONFIG}.json ${NAME}.json",
      "      sleep $(shuf -i3-15 -n1)",
      "    done",
      "    cat /pfs/trigger/trigger",
      "  ) >/pfs/out/trigger",
      "fi"
    ]
  }
}
END

echo; echo "Creating pipeline sync-deltas..."
pachctl create-pipeline -f - <<'END'
{
  "pipeline": {
    "name": "data-sync__sync-deltas"
  },
  "input": {
    "cross": [
      {
        "atom": {
          "name": "trigger",
          "repo": "data-sync__sync-lookups",
          "glob": "/"
        }
      }, {
        "atom": {
          "name": "names",
          "repo": "data-sync__delta-names",
          "glob": "/*"
        }
      }, {
        "atom": {
          "name": "scripts",
          "repo": "data-sync__git-1",
          "glob": "/"
        }
      }
    ]
  },
  "transform": {
    "env": {
      "DB_CONFIG": "dev",
      "TABLE_TYPE": "delta"
    },
    "cmd": [ "sh" ],
    "stdin": [
      "if [ -r /pfs/trigger/trigger ]; then",
      "  (",
      "    for NAME in $(ls /pfs/names); do",
      "      echo $(date) node sync-${TABLE_TYPE}.js db-config-${DB_CONFIG}.json ${NAME}.json",
      "      sleep $(shuf -i3-15 -n1)",
      "    done",
      "    cat /pfs/trigger/trigger",
      "  ) >/pfs/out/trigger",
      "fi"
    ]
  }
}
END

echo; echo "Creating pipeline merge-lookups..."
pachctl create-pipeline -f - <<'END'
{
  "pipeline": {
    "name": "data-sync__merge-lookups"
  },
  "parallelism_spec": {
    "constant": 2
  },
  "input": {
    "cross": [
      {
        "atom": {
          "name": "trigger",
          "repo": "data-sync__sync-deltas",
          "glob": "/"
        }
      }, {
        "atom": {
          "name": "names",
          "repo": "data-sync__lookup-names",
          "glob": "/*"
        }
      }, {
        "atom": {
          "name": "scripts",
          "repo": "data-sync__git-1",
          "glob": "/"
        }
      }
    ]
  },
  "transform": {
    "env": {
      "DB_CONFIG": "dev"
    },
    "cmd": [ "sh" ],
    "stdin": [
      "if [ -r /pfs/trigger/trigger ]; then",
      "  (",
      "    for name in $(ls /pfs/names); do",
      "      echo $(date) node run-tsql.js db-config-dev.json merge-${name}.tsql",
      "      sleep $(shuf -i3-15 -n1)",
      "    done",
      "    cat /pfs/trigger/trigger",
      "  ) >/pfs/out/trigger",
      "fi"
    ]
  }
}
END

echo; echo "Creating pipeline merge-deltas..."
pachctl create-pipeline -f - <<'END'
{
  "pipeline": {
    "name": "data-sync__merge-deltas"
  },
  "input": {
    "cross": [
      {
        "atom": {
          "name": "trigger",
          "repo": "data-sync__merge-lookups",
          "glob": "/"
        }
      }, {
        "atom": {
          "name": "names",
          "repo": "data-sync__delta-names",
          "glob": "/*"
        }
      }, {
        "atom": {
          "name": "scripts",
          "repo": "data-sync__git-1",
          "glob": "/"
        }
      }
    ]
  },
  "transform": {
    "env": {
      "DB_CONFIG": "dev"
    },
    "cmd": [ "sh" ],
    "stdin": [
      "if [ -r /pfs/trigger/trigger ]; then",
      "  (",
      "    for NAME in $(ls /pfs/names); do",
      "      echo $(date) node run-tsql.js db-config-${DB_CONFIG}.json merge-${NAME}.tsql",
      "      sleep $(shuf -i3-15 -n1)",
      "    done",
      "    cat /pfs/trigger/trigger",
      "  ) >/pfs/out/trigger",
      "fi"
    ]
  }
}
END